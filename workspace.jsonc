/**
 * this is the main configuration file of your bit workspace.
 * for full documentation, please see: https://docs.bit.dev/configuration
 **/{
  "$schema": "https://static.bit.dev/teambit/schemas/schema.json",
  /**
   * main configuration of the Bit workspace.
   **/
  "teambit.workspace/workspace": {
    /**
     * set the default scope when there is no matching for the component in the components array.
     **/
    "defaultScope": "watheia.app",
    /**
     * the name of the component workspace. used for development purposes.
     **/
    "name": "watheia-app",
    /**
     * set the icon to be shown on the Bit server.
     **/
    "icon": "https://cdn.watheia.org/assets/micro.png",
    /**
     * set the default directory when there is no matching for the component in the components array.
     **/
    "defaultDirectory": "watheia/apps/pwa/components/{name}",
    "vendorDirectory": "vendor"
  },
  /**
   * main configuration for component dependency resolution.
   **/
  "teambit.dependencies/dependency-resolver": {
    /**
     * choose the package manager for Bit to use. you can choose between 'npm', 'yarn', 'pnpm'
     */
    "packageManager": "teambit.dependencies/yarn",
    "policy": {
      "dependencies": {
        "@bit/bit.javascript.component.id": "^1.1.5",
        "@types/node": "^14.0.0",
        "@types/react": "^17.0.6",
        "@types/react-dom": "^17.0.5",
        "@types/yup": "^0.29.3",
        "formik": "^1.5.8",
        "typewriter-effect": "2.17.0",
        "yup": "^0.32.9",
        "@popperjs/core": "^2.0.0",
        "react": "^17.0.0",
        "react-dom": "^17.0.0",
        "@types/chai": "^4.2.11",
        "@types/lodash.debounce": "^4.0.6",
        "@types/mocha": "^8.0.0",
        "@types/react-onclickoutside": "^6.7.3",
        "@types/react-slick": "^0.23.4",
        "@types/sinon": "^10.0.0",
        "chai": "^4.2.0",
        "eslint-plugin-chai-friendly": "^0.7.1",
        "mocha": "^8.0.1",
        "next": "^10.2.0",
        "sinon": "^10.0.0",
        "@testing-library/jest-dom": "^5.11.1",
        "@testing-library/react": "^11.2.7",
        "@testing-library/user-event": "^13.1.9",
        "@types/javascript-time-ago": "^2.0.1",
        "classnames": "^2.2.6",
        "javascript-time-ago": "^2.0.13",
        "lodash.debounce": "^4.0.8",
        "node-sass": "^5.0.0",
        "number-abbreviate": "^2.0.0",
        "path-browserify": "^1.0.1",
        "rc-tooltip": "^5.1.1",
        "react-create-ref": "^1.0.1",
        "react-onclickoutside": "^6.9.0",
        "react-slick": "^0.28.1",
        "reset-css": "^5.0.1",
        "resize-observer-polyfill": "^1.5.1",
        "typescript": "^4.2.4",
        "core-js": "3.7.0",
        "graphql": "15.5.0",
        "graphql-request": "3.4.0",
        "url-parse": "1.5.1",
        "use-deep-compare": "1.1.0"
      },
      "peerDependencies": {
        "react": "^17.0.0",
        "react-dom": "^17.0.0"
      }
    },
    "packageManagerArgs": [],
    // "devFilePatterns": [
    //   "*.spec.ts"
    // ],
    /**
     * If true, then Bit will add the "--strict-peer-dependencies" option when invoking package managers.
     * This causes "bit install" to fail if there are unsatisfied peer dependencies, which is
     * an invalid state that can cause build failures or incompatible dependency versions.
     * (For historical reasons, JavaScript package managers generally do not treat this invalid
     * state as an error.)
     *
     * The default value is false to avoid legacy compatibility issues.
     * It is strongly recommended to set strictPeerDependencies=true.
     */
    "strictPeerDependencies": true,
    /**
     * map of extra arguments to pass to the configured package manager upon the installation
     * of dependencies.
     */
    "extraArgs": []
  },
  "teambit.ui-foundation/ui": {
    "port": 5000,
    // "portRange": [3003, 3100],
    "host": "0.0.0.0",
    "publicDir": "public/bit"
  },
  /**
   * automate Bit workflow with Git.
   */
  // "@teambit/git": {
  /**
   * fetch dependency and component updates on `git pull`.
   **/
  // "dependencyUpdateOnPull": true
  // },
  /**
   * workspace variants allow to set different subsets of configuration for components in your workspace.
   * this is extremely useful for upgrading, aligning and building components with a
   * new set of dependencies.
   **/
  "teambit.workspace/variants": {
    "*": {
      "teambit.dependencies/dependency-resolver": {},
      "teambit.pkg/pkg": {
        "packageManagerPublishArgs": [
          "--access public"
        ],
        "packageJson": {
          "name": "@watheia/app.{name}",
          "private": false,
          "repository": "https://gitlab.com/watheia/nx-micro.git",
          "author": "Aaron R Miller <amiller@watheia.org>",
          "license": "EPL-2.0",
          "homepage": "https://watheia.app/home",
          "keywords": [
            "watheia",
            "app",
            "micro",
            "micro-component",
            "web-component",
            "micro-frontend",
            "atomic-design",
            "material-design",
            "stencil"
          ]
        }
      },
      "teambit.react/react": {}
    },
    "app-ui/src/hooks/use-graphql-light": {
      "teambit.dependencies/dependency-resolver": {
        "policy": {
          "dependencies": {
            "graphql": "-"
          },
          "peerDependencies": {
            "graphql": "^14.0.0 || ^15.0.0"
          }
        }
      }
    }
  }
}