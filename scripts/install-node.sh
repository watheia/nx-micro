#!/usr/bin/env bash

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
nvm install --lts

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

nvm install --lts
nvm use --lts
npm install --global npm yarn @teambit/bvm

export PATH=$PATH:$HOME/bin
# All must pass for script to pass
bvm install && \
  bit config set analytics_reporting false && \
  bit config set error_reporting false && \
  bit config set no_warnings true