import React, { HTMLAttributes } from 'react';
import classNames from 'classnames';

import { Paragraph } from '@watheia/app.ui.text.paragraph';
import { PossibleSizes } from '@watheia/app.ui.theme.sizes';
import { mutedText } from '@watheia/app.ui.text.muted-text';

import { H2 } from '@watheia/app.elements.heading';
import { textColumn } from '@watheia/app.ui.layout.page-frame';
import { marginCenter } from '@watheia/app.ui.layout.align';

/**
 * Title and description to the Bit integrations ecosystem.
 * @name EcoSystem
 */
export const EcoSystem = (props: HTMLAttributes<HTMLDivElement>) => {
	return (
		<div {...props} data-bit-id="watheia.app/sections/enterprise-offering/eco-system">
			<H2 size={PossibleSizes.sm}>Rooted in your ecosystem</H2>
			<Paragraph
				className={classNames(mutedText, textColumn, marginCenter)}
				size={PossibleSizes.lg}
			>
				Bit integrates into your software-building toolchain and plays with your
				ecosystem. Get advanced and custom integrations to boost delivery.
			</Paragraph>
		</div>
	);
};
