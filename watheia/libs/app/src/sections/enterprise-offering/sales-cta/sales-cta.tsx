import React, { HTMLAttributes } from 'react';
import classNames from 'classnames';

import { Card } from '@watheia/app.ui.surfaces.card';
import { PossibleSizes } from '@watheia/app.ui.theme.sizes';
import { mutedText } from '@watheia/app.ui.text.muted-text';
import { TextSeparator } from '@watheia/app.ui.text.text-separator';
import { fullWidth, marginCenter } from '@watheia/app.ui.layout.align';

import { Button } from '@watheia/app.elements.button';
import { H4 } from '@watheia/app.elements.heading';

import styles from './sales-cta.module.scss';
import { margin } from '@watheia/app.layout.experimental.spacing';
import { ContactForm } from './contact-form';
import { ContactValues } from './contact-values';
import { Paragraph } from '@watheia/app.ui.text.paragraph';
import { colorPalette } from '@watheia/app.ui.theme.color-palette';

export type EnterpriseCtaProps = {
	/** handles form submission, with user input as a single object parameter. Return a promise to show loader */
	onSubmitCta?: (values: ContactValues) => any | Promise<any>;
	/** handles 'book a meeting' call to action. Return a promise to show loader */
	onBookMeeting?: () => any | Promise<any>;
} & HTMLAttributes<HTMLDivElement>;

/**
 * A call-to-action section, allowing enterprise users to contact us directly from the Enterprise page.<br/>
 * Form is managed by Formik, and validated by Yup.
 * @param SalesCta
 */
export const SalesCta = (props: EnterpriseCtaProps) => {
	const { onSubmitCta, onBookMeeting, className, ...rest } = props;
	return (
		<div
			{...rest}
			className={classNames(styles.particlesBg, className)}
			data-bit-id="watheia.app/sections/enterprise-offering/sales-cta"
		>
			<Card id="lets-talk" className={classNames(styles.formCard, marginCenter)}>
				<H4 size={PossibleSizes.xs}>Let’s talk </H4>
				<Paragraph className={classNames(margin[30], mutedText)}>
					Drop a message and we’ll get back to you shortly.
				</Paragraph>

				<ContactForm onSubmitMessage={onSubmitCta} />

				<TextSeparator
					className={classNames(mutedText, styles.margin, styles.separator)}
				>
					OR
				</TextSeparator>

				<Button
					importance="normal"
					onClick={onBookMeeting}
					className={classNames(fullWidth, colorPalette.muted)}
				>
					Book Intro
				</Button>
			</Card>

			<div className={styles.particlesContainer}>
				<div className={styles.redParticle} />
			</div>
		</div>
	);
};
