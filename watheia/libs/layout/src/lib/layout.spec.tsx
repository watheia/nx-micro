import { render } from "@testing-library/react"

import { withLayout } from "@watheia/app.layout"

describe("app.layout.withLayout", () => {
  it("should render successfully", () => {
    const RawComp = () => <p>Hello, World!</p>
    const WrappedComp = withLayout(RawComp)
    const { baseElement } = render(<WrappedComp />)
    expect(baseElement).toBeTruthy()
  })
})
