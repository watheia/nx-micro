import * as React from "react"
import Head from "next/head"
import { AppProps } from "next/app"
import { ThemeProvider } from "@material-ui/core/styles"
import { CacheProvider } from "@emotion/react"
import CssBaseline from "@material-ui/core/CssBaseline"
import createCache from "@emotion/cache"
import theme from "./theme"

export type LayoutProps = Record<string, unknown>

export const cache = createCache({ key: "css", prepend: true })

export function withLayout<P>(Component: React.ComponentType<P>) {
  return function WithRoot(props: LayoutProps) {
    React.useEffect(() => {
      // Remove the server-side injected CSS.
      document.body.classList?.remove("loading")
      const jssStyles = document.querySelector("#jss-server-side")
      if (jssStyles && jssStyles.parentElement) {
        jssStyles.parentElement.removeChild(jssStyles)
      }
    }, [])
    return (
      <CacheProvider value={cache}>
        <Head>
          <title>Watheia Labs</title>
          <meta name="viewport" content="initial-scale=1, width=device-width" />
          <link rel="stylesheet" href="https://use.typekit.net/xyi1bqv.css" />
        </Head>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Component {...pageProps} />
        </ThemeProvider>
      </CacheProvider>
    )
  }
}

export default withLayout
