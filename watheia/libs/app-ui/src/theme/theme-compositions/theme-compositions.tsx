import React from 'react';
import { EvaIconFont } from '@watheia/app.ui.theme.icon-font';
import { ThemeDocumenter, ThemeDocumenterProps } from '@watheia/app.ui.theme.theme-context';

const ICON_MOON_VERSION = 'mxd7i0';

export type ThemeCompositionsProps = {} & ThemeDocumenterProps;

export const ThemeCompositions = ({ children, ...rest }: ThemeCompositionsProps) => {
	return (
		<ThemeDocumenter {...rest}>
			<EvaIconFont query={ICON_MOON_VERSION} />
			{children}
		</ThemeDocumenter>
	);
};
