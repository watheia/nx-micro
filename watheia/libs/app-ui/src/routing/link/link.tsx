import React from 'react';
import type { LinkProps } from '@watheia/app.ui.routing.native-link';
import { useRouting } from '@watheia/app.ui.routing.routing-provider';

export type { LinkProps };

export function Link(props: LinkProps) {
	const ActualLink = useRouting().Link;
	return <ActualLink {...props} />;
}
